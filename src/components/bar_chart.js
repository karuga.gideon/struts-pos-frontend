import React from "react";
import { Bar } from "react-chartjs-2";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);

const options = {
  responsive: true,
  plugins: {
    legend: {
      position: "top",
    },
    title: {
      display: true,
      text: "Sales",
    },
  },
};

function BarChart(props) {
  const data = {
    labels: props.labels,
    datasets: [
      {
        label: "Customers Count",
        data: props.datasets[0].data,
        backgroundColor: "#3e8ed0",
      },
      {
        label: "Total Sales",
        data: props.datasets[0].data,
        backgroundColor: "#00d1b2",
      },
    ],
  };

  return (
    <>
      <Bar data={data} options={options} /> <br />
      <br />
    </>
  );
}

export default BarChart;

export const API_BASE_URL = process.env.API_BASE_UR;
export const PROFILE_PAYLOAD = "PROFILE_PAYLOAD";
export const ISSERVER = typeof window === "undefined";
export const USER_ID = !ISSERVER && localStorage.getItem("user_id");
export const ACCESS_TOKEN =
  !ISSERVER && localStorage.getItem("strutspos_access_token");

import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import WebNavbar from "../../components/web_navbar";
import DocumentationSideMenu from "../../components/documentation_side_menu";

const APIKeysDocumentation = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <>
      <WebNavbar />
      <section className="section">
        <div className="container">
          <div className="columns">
            <div className="column is-one-quarter">
              <DocumentationSideMenu />
            </div>
            <div
              className="column is-three-quarters"
              style={{ marginBottom: "12px", marginTop: "-10px" }}
            >
              <h2 className="title">API Keys</h2>

              <p className="">
                In the{" "}
                <Link to="/api-keys" style={{ color: "#00d1b2" }}>
                  API Keys
                </Link>{" "}
                section, you can setup keys that you can use in your API
                requests.
              </p>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default APIKeysDocumentation;

import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import WebNavbar from "../../components/web_navbar";
import DocumentationSideMenu from "../../components/documentation_side_menu";

const Documentation = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <>
      <WebNavbar />
      <section className="section">
        <div className="container">
          <div className="columns">
            <div className="column is-one-quarter">
              <DocumentationSideMenu />
            </div>
            <div
              className="column is-three-quarters"
              style={{ marginBottom: "12px", marginTop: "-10px" }}
            >
              <h2 className="title">Getting Started</h2>
              <p className="">
                To get started, you will need to create an account on the
                platform. You can do so by visitng the&nbsp;
                <Link to="/signup" style={{ color: "#00d1b2" }}>
                  sign up
                </Link>{" "}
                page. Once your account has been setup successfully, you can
                proceed to{" "}
                <Link to="/login" style={{ color: "#00d1b2" }}>
                  login.
                </Link>{" "}
                We also offer a custom on premise setup whereby we can customize
                the data input locations, either via files or database
                connection, so that you can use your existing data with AI as
                is.
              </p>
              <br />

              <p className="">
                Once logged in, you will be able to see a{" "}
                <Link to="/dashboard" style={{ color: "#00d1b2" }}>
                  dashboard
                </Link>{" "}
                similar to below. Initially, the numbers will read 0 before you
                do AI trainings and setup API keys. The values will increment as
                you continue to use the system.
              </p>
              <br />
              <br />
              <img src="/assets/dashboard.png" alt="Dashboard" />
              <br />
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default Documentation;

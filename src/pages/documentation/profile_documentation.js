import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import WebNavbar from "../../components/web_navbar";
import DocumentationSideMenu from "../../components/documentation_side_menu";

const ProfileDocumentation = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <>
      <WebNavbar />
      <section className="section">
        <div className="container">
          <div className="columns">
            <div className="column is-one-quarter">
              <DocumentationSideMenu />
            </div>
            <div
              className="column is-three-quarters"
              style={{ marginBottom: "12px", marginTop: "-10px" }}
            >
              <h2 className="title">Profile</h2>

              <p className="">
                In the{" "}
                <Link to="/profile" style={{ color: "#00d1b2" }}>
                  profile
                </Link>{" "}
                section, you can view the configured user and business details.
              </p>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default ProfileDocumentation;

import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import AdminNavbar from "../../../components/admin_navbar";
import AdminSideMenu from "../../../components/admin_side_menu";
import { useNavigate } from "react-router-dom";
import axios from "axios";

const Item = () => {
  let { id } = useParams();

  const navigate = useNavigate();

  const [item, setItem] = useState({});

  var apiBaseURL = process.env.REACT_APP_API_BASE_URL;
  // var pdfFile = apiBaseURL + "/items/" + id + "/pdf";

  useEffect(() => {
    let accessToken = localStorage.getItem("strutspos_access_token");
    if (!accessToken) {
      navigate("/");
    }

    window.scrollTo(0, 0);

    var url = apiBaseURL + "/items/" + id;

    const config = {
      headers: {
        "x-strutspos-token": accessToken,
        "x-strutspos-application": "user",
      },
    };

    axios
      .get(url, config)
      .then((response) => {
        let itemData = JSON.parse(JSON.stringify(response.data));
        setItem(itemData);
      })
      .catch((err) => console.error("fetchItems err = " + err.message));
  }, [apiBaseURL, id, navigate]);

  return (
    <>
      <AdminNavbar />

      <section className="section">
        <div className="container">
          <h1 className="title">Item: {item.item_number}</h1>

          <div className="columns">
            <div className="column is-one-quarter">
              <AdminSideMenu />
            </div>
            <div
              className="column is-three-quarters"
              style={{ borderTop: "1px solid #C8C8C8" }}
            >
              <div className="clearfix"></div>
              <p>
                <b>Item #{item.name}</b>
              </p>
              <br />
              Name: {item.name}
              <br />
              Description: {item.description}
              <br />
              Cost Price: {item.cost_price}
              <br />
              Retail Price: {item.retail_price}
              <br />
              Created At: {item.created_at}
              <br />
              <br />
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default Item;

import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import WebFooter from "../components/web-footer";
import WebNavbar from "../components/web_navbar";

const Home = () => {
  const navigate = useNavigate();

  useEffect(() => {
    let accessToken = localStorage.getItem("strutspos_access_token");
    if (accessToken) {
      navigate("/dashboard");
    }

    window.scrollTo(0, 0);
  }, [navigate]);

  return (
    <>
      <WebNavbar />
      <section className="section">
        <div className="container">
          <h1 className="title">Optimus</h1>
          <p className="subtitle">
            Electronic Signature Device signing, reporting and analytics tool.
          </p>
          <p>
            <br />
            by{" "}
            <a
              href="https://www.strutstechnology.co.ke"
              // style="color:black; text-decoration: none; font-weight: bold;"
              target="_blank"
              rel="noreferrer"
            >
              Struts Technology
            </a>
          </p>
        </div>
      </section>
      <WebFooter />
    </>
  );
};

export default Home;

import axios from "axios";
import { createChatBotMessage } from "react-chatbot-kit";
import { createClientMessage } from "react-chatbot-kit";

class MessageParser {
  constructor(actionProvider, state) {
    this.actionProvider = actionProvider;
    this.state = state;
  }

  chatAI = (message) => {
    var apiBaseURL = process.env.REACT_APP_AI_API_BASE_URL;

    axios({
      url: `${apiBaseURL}/chatbot`,
      method: "POST",
      withCredentials: false,
      headers: {
        "Content-Type": "application/json",
        "x-strutsai-application": "user",
        "x-client-identifier": "web",
        "struts-api-key": "92aa2b31-08d0-4200-b5e4-0f53d960bb47",
      },
      data: {
        client: "struts",
        input_text: message,
      },
    })
      .then((response) => {
        if (response.data) {
          console.log("AI Response: " + response.data);
          console.log("AI Response message: " + response.data.response);
          createChatBotMessage(response.data.response);
        } else {
          console.log("Error processing AI Response");
        }
      })
      .catch((error) => {
        console.log("Error processing AI Response error: " + error);
      });
  };

  parse(message) {
    console.log(message);

    createClientMessage("hELLO");
    createChatBotMessage("Hi");

    // this.setState((prev) => ({
    //   ...prev,
    //   messages: [...prev.messages, "Hey there"],
    // }));
  }
}

export default MessageParser;

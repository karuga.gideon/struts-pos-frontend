import React from "react";

const AIMessageParser = ({ children, actions }) => {
  const parse = (message) => {
    actions.handleHello(message);
  };

  return (
    <div>
      {React.Children.map(children, (child) => {
        return React.cloneElement(child, {
          parse: parse,
          actions: {},
        });
      })}
    </div>
  );
};

export default AIMessageParser;

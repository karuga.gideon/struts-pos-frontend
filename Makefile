deploy:	
	unzip build.zip -d .
	
	sudo rm -rf /var/www/pos.strutstechnology.co.ke
	sudo mkdir /var/www/pos.strutstechnology.co.ke
	sudo cp -r ./build/* /var/www/pos.strutstechnology.co.ke

	
	sudo rm -rf /var/www/pos.strutstechnology.com
	sudo mkdir /var/www/pos.strutstechnology.com
	sudo cp -r ./build/* /var/www/pos.strutstechnology.com

	
	sudo rm -rf /var/www/optimus.strutstechnology.com
	sudo mkdir /var/www/optimus.strutstechnology.com
	sudo cp -r ./build/* /var/www/optimus.strutstechnology.com

deploy-prod: 	
	npm install
	npm run build
	tar -a -c -f build.zip ./build
	scp -r build.zip gidemn@139.162.207.36:/home/gidemn/Apps/StrutsPOS/struts-pos-frontend/build.zip

install:
	npm install 

server: 
	npm start 

zip: 
	tar -a -c -f build.zip ./build